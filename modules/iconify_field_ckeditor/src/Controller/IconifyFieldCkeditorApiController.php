<?php

namespace Drupal\iconify_field_ckeditor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\iconify_field\Service\IconResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Get iconify icons of a collection.
 */
class IconifyFieldCkeditorApiController extends ControllerBase {

  /**
   * The IconResolver service instance.
   *
   * @var \Drupal\iconify_field\Service\IconResolverInterface
   */
  protected $iconResolver;

  /**
   * The renderer service instance.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * IconifyFieldApiController constructor.
   */
  public function __construct(
    IconResolverInterface $iconResolver,
    RendererInterface $renderer,
  ) {
    $this->iconResolver = $iconResolver;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('iconify_field.icon_resolver'),
      $container->get('renderer'),
    );
  }

  /**
   * Get iconify icons.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $icon
   *   The icon collection and name, separated by a colon.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response.
   */
  public function icon(Request $request, string $icon): Response {
    $icon_render = $this->iconResolver->getIcon($icon, [], TRUE);
    $icon = $this->renderer->renderRoot($icon_render);

    return new Response($icon, 200, [
      'Content-Type' => 'image/svg+xml',
    ]);
  }

}
