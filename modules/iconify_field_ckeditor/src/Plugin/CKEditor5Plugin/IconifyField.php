<?php

declare(strict_types=1);

namespace Drupal\iconify_field_ckeditor\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\EditorInterface;
use Drupal\iconify_field\Service\IconResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CKEditor 5 Line Height plugin configuration.
 */
class IconifyField extends CKEditor5PluginDefault implements ContainerFactoryPluginInterface, CKEditor5PluginConfigurableInterface {

  use CKEditor5PluginConfigurableTrait;

  /**
   * The IconResolver service instance.
   *
   * @var \Drupal\iconify_field\Service\IconResolverInterface
   */
  protected $iconResolver;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    IconResolverInterface $icon_resolver,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->iconResolver = $icon_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('iconify_field.icon_resolver'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'default_collection' => NULL,
      'collections' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $collections = $this->iconResolver->getIconifyCollections();
    $config = $this->configuration;

    $form['default_collection'] = [
      '#type' => 'select',
      '#title' => $this->t('Default collection'),
      '#description' => $this->t('The default collection to show.'),
      '#default_value' => $config['default_collection'],
      '#options' => [
        NULL => $this->t('First available'),
        ...$collections,
      ],
    ];

    $form['collections'] = [
      '#type' => 'select',
      '#title' => $this->t('Allowed collections'),
      '#description' => $this->t('Select the icon collections that can be used. If none are selected, all collections will be available.'),
      '#default_value' => $config['collections'],
      '#options' => $collections,
      '#multiple' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['default_collection'] = $form_state->getValue('default_collection') ?? NULL;
    $this->configuration['collections'] = $form_state->getValue('collections') ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    return [
      'iconifyField' => [
        'defaultCollection' => $this->configuration['default_collection'] ?? NULL,
        'collections' => implode(',', $this->configuration['collections'] ?? []),
      ],
    ];
  }

}
