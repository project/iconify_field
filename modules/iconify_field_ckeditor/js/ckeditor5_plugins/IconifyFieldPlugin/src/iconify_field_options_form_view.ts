import type { Editor } from 'ckeditor5/src/core'
import type { FocusableView } from 'ckeditor5/src/ui'
import { ButtonView, LabeledFieldView, SwitchButtonView, View, createLabeledInputText, submitHandler } from 'ckeditor5/src/ui'
import { icons } from 'ckeditor5/src/core'
import type Element from '@ckeditor/ckeditor5-engine/src/model/element'

export default class IconifyFieldOptionsFormView extends View {
  private _classInput: LabeledFieldView

  private _decorativeToggle: SwitchButtonView
  private _ariaNameInput: LabeledFieldView

  private _saveButtonView: ButtonView
  private _cancelButtonView: ButtonView

  constructor(locale: Editor['locale']) {
    super(locale)

    this._classInput = this._createClassInputView()

    this._decorativeToggle = this._decorativeToggleView()
    this._ariaNameInput = this._createAriaNameInputView()

    this._saveButtonView = this._createButton(
      Drupal.t('Save'),
      icons.check,
      'ck-button-save',
    )

    this._saveButtonView.type = 'submit'
    this._saveButtonView.bind('isEnabled').to(
      this._decorativeToggle,
      'isOn',
      this._ariaNameInput,
      'isEmpty',
      (isDecorativeToggleOn, isLabeledInputEmpty) =>
        isDecorativeToggleOn || !isLabeledInputEmpty,
    )

    this._cancelButtonView = this._createButton(
      Drupal.t('Cancel'),
      icons.cancel,
      'ck-button-cancel',
      'cancel',
    )

    this.setTemplate({
      tag: 'form',

      attributes: {
        class: [
          'ck',
          'ck-link-form',
          'ck-vertical-form',
          'ck-link-form_layout-vertical',
        ],
        tabindex: '-1',
      },

      children: [
        this._classInput,

        {
          tag: 'div',
          attributes: {
            class: [
              'ck',
            ],
          },
          children: [
            this._decorativeToggle,
          ],
        },
        this._ariaNameInput,

        this._saveButtonView,
        this._cancelButtonView,
      ],
    })
  }

  override render() {
    super.render()

    submitHandler({ view: this })
  }

  public reset(element: Element) {
    const elAttributes = element.getAttributes()
    const attributes: { [key: string]: any } = {}

    for (const [key, value] of elAttributes)
      attributes[key] = value

    const classes = attributes.class.replace('iconify-field', '').trim()

    this._setInputValue(this._classInput.fieldView, classes)

    this._decorativeToggle.isOn = !!(attributes['aria-hidden'] ?? false)
    this._setInputValue(this._ariaNameInput.fieldView, attributes['aria-label'] ?? '')
  }

  private _setInputValue(input: FocusableView, value: string) {
    ;(input as unknown as HTMLInputElement).value = value

    if (input.element)
      (input.element as HTMLInputElement).value = value
  }

  public getClassInputValue() {
    const classes = (this._classInput.fieldView.element as HTMLInputElement)?.value
    return `iconify-field ${classes}`
  }

  public isDecorativeToggleOn() {
    return this._decorativeToggle.isOn
  }

  public getAriaNameInputValue() {
    return (this._ariaNameInput.fieldView.element as HTMLInputElement)?.value
  }

  private _decorativeToggleView() {
    const decorativeToggle = new SwitchButtonView(this.locale)

    decorativeToggle.set({
      withText: true,
      label: Drupal.t('Decorative image'),
      class: [
        'ck-labeled-field-view',
      ],
    })

    decorativeToggle.on('execute', () => {
      decorativeToggle.set('isOn', !decorativeToggle.isOn)
    })

    return decorativeToggle
  }

  private _createClassInputView() {
    const classInput = new LabeledFieldView(
      this.locale,
      createLabeledInputText,
    )

    classInput.label = Drupal.t('Additional classes')

    return classInput
  }

  private _createAriaNameInputView() {
    const ariaNameInput = new LabeledFieldView(
      this.locale,
      createLabeledInputText,
    )

    ariaNameInput
      .bind('class')
      .to(this._decorativeToggle, 'isOn', value => (value ? 'ck-hidden' : ''))
    ariaNameInput.label = Drupal.t('Accessible name')

    return ariaNameInput
  }

  private _createButton(
    label: string,
    icon: string,
    className: string,
    eventName?: string | ((name: string) => string),
  ) {
    const button = new ButtonView(this.locale)

    button.set({
      label,
      icon,
      tooltip: true,
    })

    button.extendTemplate({
      attributes: {
        class: className,
      },
    })

    if (eventName)
      button.delegate('execute').to(this, eventName)

    return button
  }
}
