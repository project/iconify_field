import type { Model, Writer } from '@ckeditor/ckeditor5-engine'
import { Command } from 'ckeditor5/src/core'

interface IconifyFieldConfig {
  defaultCollection?: string
  collections?: string
}

interface InsertIconifyFieldCommandOptions {
  icon: string
  attributes: {
    [key: string]: any
  }
}

export default class InsertIconifyFieldCommand extends Command {
  override execute(options?: InsertIconifyFieldCommandOptions) {
    const { model } = this.editor

    if (options?.icon) {
      model.change((writer) => {
        this._insertIconifyField(model, writer, options.icon, options.attributes)
      })

      return
    }

    const config = this.editor.config.get('iconifyField') as IconifyFieldConfig

    const query = new URLSearchParams({
      default_collection: config.defaultCollection ?? '',
      collections: config.collections ?? '',
    })

    const modal = Drupal.ajax({
      url: `/admin/iconify_field/menu-icons/picker?${query}`,
      dialogType: 'modal',
      dialog: {
        height: 600,
        width: 800,
      },
    })

    modal.execute()

    document.body.addEventListener('iconify-field-pick-icon', (event: any) => {
      const closeBtn = document.body.querySelector('.ui-dialog-titlebar-close')
      if (closeBtn)
        (closeBtn as HTMLElement).click()

      const icon = event.detail.text

      model.change((writer) => {
        this._insertIconifyField(model, writer, icon, {
          'class': 'iconify-field',
          'aria-hidden': 'true',
        })
      })
    })
  }

  override refresh() {
    const { model } = this.editor
    const { selection } = model.document

    const position = selection.getFirstPosition()

    if (!position)
      return

    const allowedIn = model.schema.findAllowedParent(
      position,
      'iconifyField',
    )

    this.isEnabled = allowedIn !== null
  }

  private _createIconifyField(
    writer: Writer,
    icon?: string,
    attributes?: { [key: string]: any },
  ) {
    const iconifyField = writer.createElement('iconifyField', {
      'data-icon': icon,
      ...attributes,
    })

    return iconifyField
  }

  private _insertIconifyField(
    model: Model,
    writer: Writer,
    icon?: string,
    extraAttributes?: { [key: string]: unknown },
  ) {
    const attributesIterator = model.document.selection.getAttributes()

    const attributes = extraAttributes ?? {}

    ;[...attributesIterator].forEach((attr) => {
      attributes[attr[0]] = attr[1]
    })

    model.insertObject(
      this._createIconifyField(writer, icon, attributes),
      null,
      null,
      {
        setSelection: 'on',
      },
    )
  }
}
