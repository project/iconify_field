import { Plugin } from 'ckeditor5/src/core'
import { ButtonView, ContextualBalloon, clickOutsideHandler } from 'ckeditor5/src/ui'
import type { ViewDocumentClickEvent } from '@ckeditor/ckeditor5-engine'
import icon from '../img/IconifyField.svg'
import IconifyFieldOptionsFormView from './iconify_field_options_form_view'
import type InsertIconifyFieldCommand from './insert_iconify_field_command'

export default class IconifyFieldUI extends Plugin {
  private _balloon!: ContextualBalloon
  private _form!: IconifyFieldOptionsFormView

  static get requires() {
    return [ContextualBalloon]
  }

  init() {
    const { editor } = this

    this._createFormBalloon()

    editor.ui.componentFactory.add('iconifyField', (locale) => {
      const buttonView = new ButtonView(locale)

      buttonView.set({
        label: editor.t('Icon'),
        icon,
        tooltip: true,
      })

      this.listenTo(buttonView, 'execute', () =>
        editor.execute('insertIconifyField'))

      return buttonView
    })

    this.listenTo(this._form, 'submit', () => {
      const { editor } = this
      const { selection } = editor.model.document

      const element = selection.getSelectedElement()

      if (!element)
        return

      const insertParams: Parameters<InsertIconifyFieldCommand['execute']>[0] = {
        icon: (element.getAttribute('data-icon') as string) ?? '',
        attributes: {},
      }

      if (!insertParams)
        return

      insertParams.attributes.class = this._form.getClassInputValue()

      if (this._form.isDecorativeToggleOn()) {
        insertParams.attributes['aria-hidden'] = 'true'
      }
      else {
        insertParams.attributes['aria-label'] = this._form.getAriaNameInputValue()
        insertParams.attributes.role = 'img'
      }

      editor.execute('insertIconifyField', insertParams)
    })

    this.listenTo(this._form, 'cancel', () => {
      this._hideUI()
    })

    editor.model.document.on('change:data', () => {
      this._hideUI()
    })
  }

  private _createFormBalloon() {
    const { editor } = this
    const viewDocument = editor.editing.view.document

    this._balloon = this.editor.plugins.get(ContextualBalloon)
    this._form = new IconifyFieldOptionsFormView(this.editor.locale)

    this.listenTo<ViewDocumentClickEvent>(viewDocument, 'click', () => {
      const iconifyField = this._getSelectedIconifyFieldElement()

      if (iconifyField)
        this._showUI()
    })

    clickOutsideHandler({
      emitter: this._form,
      activator: () => this._balloon.hasView(this._form),
      contextElements: [this._balloon.view.element as HTMLElement],
      callback: () => this._hideUI(),
    })
  }

  private _showUI() {
    const { editor } = this
    const { view } = editor.editing
    const { selection } = editor.model.document

    const element = selection.getSelectedElement()

    if (element)
      this._form.reset(element)

    const range = view.document.selection.getFirstRange()

    if (!range)
      return

    const position = view.domConverter.viewRangeToDom(range)

    this._balloon.add({
      view: this._form,
      position: {
        target: position,
      },
    })
  }

  private _hideUI() {
    this.editor.editing.view.focus()

    if (this._balloon.hasView(this._form))
      this._balloon.remove(this._form)
  }

  private _getSelectedIconifyFieldElement() {
    const { selection } = this.editor.editing.view.document
    const selectedElement = selection.getSelectedElement()

    if (!selectedElement?.hasClass('iconify-field') || !selectedElement.hasAttribute('data-icon'))
      return null

    return selectedElement ?? null
  }
}
