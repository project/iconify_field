import { Plugin } from 'ckeditor5/src/core'
import { Widget, toWidget } from 'ckeditor5/src/widget'
import type { DowncastWriter, Element } from '@ckeditor/ckeditor5-engine'
import InsertIconifyFieldCommand from './insert_iconify_field_command'

export default class IconifyFieldEditing extends Plugin {
  static get requires() {
    return [Widget]
  }

  init() {
    this._defineSchema()
    this._defineConverters()

    this.editor.commands.add(
      'insertIconifyField',
      new InsertIconifyFieldCommand(this.editor),
    )
  }

  _defineSchema() {
    const { schema } = this.editor.model

    schema.register('iconifyField', {
      inheritAllFrom: '$inlineObject',
      allowAttributes: [
        'class',
        'data-icon',
        'aria-hidden',
        'aria-label',
        'role',
      ],
    })
  }

  _defineConverters() {
    const { conversion } = this.editor

    conversion.for('upcast').elementToElement({
      model: (viewElement, { writer }) => {
        const attributes = this._parseAttributes(viewElement.getAttributes())

        return writer.createElement('iconifyField', {
          'data-icon': viewElement.getAttribute('data-icon'),
          ...attributes,
        })
      },
      view: {
        name: 'span',
        classes: 'iconify-field',
      },
    })

    conversion.for('dataDowncast').elementToElement({
      model: 'iconifyField',
      view: (modelElement, { writer }) => {
        return this._createIconifyFieldView(modelElement, writer)
      },
    })

    conversion.for('editingDowncast').elementToElement({
      model: 'iconifyField',
      view: (modelElement, { writer }) => {
        const element = this._createIconifyFieldView(modelElement, writer)

        return toWidget(element, writer)
      },
    })
  }

  private _createIconifyFieldView(modelElement: Element, writer: DowncastWriter) {
    const icon = modelElement.getAttribute('data-icon') ?? ''
    const attributes = this._parseAttributes(modelElement.getAttributes())

    const element = writer.createContainerElement('span', {
      'class': 'iconify-field',
      'data-icon': icon,
      ...attributes,
    })

    element._appendChild(writer.createRawElement('span', {
      class: 'iconify-field--icon',
      style: icon
        ? `mask-image: url(/api/iconify_field_ckeditor/icon/${icon})`
        : undefined,
    }))

    return element
  }

  private _parseAttributes(elAttributes: IterableIterator<[string, unknown]>) {
    const attributes: { [key: string]: any } = {}

    for (const [key, value] of elAttributes)
      attributes[key] = value

    return attributes
  }
}
