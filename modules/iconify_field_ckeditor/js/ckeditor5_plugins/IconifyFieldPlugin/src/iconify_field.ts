import { Plugin } from 'ckeditor5/src/core'
import IconifyFieldEditing from './iconify_field_editing'
import IconifyFieldUI from './iconify_field_ui'

export default class IconifyField extends Plugin {
  static get requires() {
    return [
      IconifyFieldEditing,
      IconifyFieldUI,
    ]
  }
}
