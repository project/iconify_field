<?php

/**
 * @file
 * Provides common hooks for the Iconify Field: CKEditor module.
 */

use Drupal\Core\Render\Markup;

/**
 * Implements hook_preprocess_field().
 */
function iconify_field_ckeditor_preprocess_field(&$variables) {
  if (!$variables['items']) {
    return;
  }

  foreach ($variables['items'] as $key => &$element) {
    $content = &$element['content'];

    if (gettype($key) !== 'integer') {
      continue;
    }

    if (!isset($content['#type']) || $content['#type'] !== 'processed_text') {
      continue;
    }

    if (strpos($content['#text'], 'iconify-field') === FALSE) {
      continue;
    }

    $renderer = \Drupal::service('renderer');
    $renderer->renderRoot($content);

    $content['#markup'] = preg_replace_callback(
      '/<span (class="iconify-field.*?") data-icon="([^"]+?)"( .+?|)>.+?<\/span><\/span>/',
      function ($matches) use ($renderer) {
        $icon_name = $matches[2];
        $attrs = trim($matches[1]);

        if ($matches[3]) {
          $attrs = $attrs . ' ' . $matches[3];
        }

        $params = _iconify_field_ckeditor_get_params_from_attributes($attrs);

        $icon_render = [
          '#type' => 'iconify_icon',
          '#icon' => $icon_name,
          '#attributes' => $params,
        ];

        $icon = $renderer->renderRoot($icon_render);

        // Remove newlines from the icon, so no spaces appear when
        // Twig debugging is enabled.
        $icon = str_replace("\n", '', $icon);

        return $icon;
      },
      $content['#markup'],
    );

    $content['#markup'] = Markup::create($content['#markup']);
    $content['#children'] = $content['#markup'];
  }
}

/**
 * Transform a string of HTML attributes to an array.
 *
 * @param string $attributes
 *   The string of attributes to transform.
 *
 * @return array
 *   Return the array of attributes.
 */
function _iconify_field_ckeditor_get_params_from_attributes(string $attributes) : array {
  $params = [];

  $attr_array = [];
  preg_match_all('/.+?=".+?"/', $attributes, $attr_array);

  foreach ($attr_array[0] as $item) {
    $item = trim($item);
    $matches = [];
    preg_match('/^(.+?)="(.+?)"/', $item, $matches);

    if (count($matches) < 3) {
      continue;
    }

    $params[$matches[1]] = $matches[2];
  }

  return $params;
}
