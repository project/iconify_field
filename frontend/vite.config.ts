import { URL, fileURLToPath } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite'
import AutoImport from 'unplugin-auto-import/vite'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    Components({
      dts: true,
      directoryAsNamespace: true,
      dirs: [
        'src/components',
      ],
    }),
    AutoImport({
      dts: true,
      include: [
        /\.[tj]sx?$/,
        /\.vue$/,
        /\.vue\?vue/,
      ],
      imports: [
        'vue',
        '@vueuse/core',
      ],
      dirs: [
        './src/composables',
      ],
    }),
  ],
  build: {
    target: 'esnext',
    manifest: false,
    rollupOptions: {
      input: '/src/main.ts',
      output: {
        dir: 'dist',
        entryFileNames: 'index.js',
        format: 'iife',
      },
    },
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
})
