import { createApp } from 'vue'
import App from './App.vue'

function init() {
  const app = createApp(App)

  // @ts-expect-error Drupal is a global variable
  app.config.globalProperties.Drupal = Drupal

  app.mount('#iconify-field--app')
}

// Observe the body for changes and initialize the app
// when the #iconify-field--app element is added.
const observer = new MutationObserver((mutations) => {
  mutations.forEach((mutation) => {
    // Only process added nodes
    if (!mutation.addedNodes || !mutation.addedNodes.length)
      return

    mutation.addedNodes.forEach((node) => {
      // Only process HTMLElement nodes
      if (!(node instanceof HTMLElement) || node.tagName === 'SCRIPT')
        return

      // Only process nodes with the #iconify-field--app selector
      if (node.id === 'drupal-modal' || !node.querySelector('#iconify-field--app'))
        return

      init()
    })
  })
})

observer.observe(document.body, {
  childList: true,
  subtree: false,
  attributes: true,
  characterData: false,
})

// Initialize the app if the #iconify-field--app element is already present
// E.g. when on the modal page directly.
if (document.querySelector('#iconify-field--app'))
  init()
