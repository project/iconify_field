import { describe, expect, it } from 'vitest'

import { flushPromises, mount } from '@vue/test-utils'
import SearchForm from '../SearchForm.vue'

describe('app', () => {
  it('renders properly', async () => {
    const wrapper = mount(SearchForm)

    expect(wrapper.findComponent({ name: 'SearchBar' }).exists()).toBe(true)

    // These components are lazy-loaded, so they won't be available immediately.
    expect(wrapper.findComponent({ name: 'CollectionDropdown' }).exists()).toBe(false)
    expect(wrapper.findComponent({ name: 'IconOverview' }).exists()).toBe(false)

    await flushPromises()

    expect(wrapper.findComponent({ name: 'CollectionDropdown' }).exists()).toBe(true)

    // This component should still not be available,
    // since it's only loaded when the collections have been fetched.
    expect(wrapper.findComponent({ name: 'IconOverview' }).exists()).toBe(false)
  })
})
