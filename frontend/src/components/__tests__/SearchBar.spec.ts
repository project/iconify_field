import { describe, expect, it } from 'vitest'

import { mount } from '@vue/test-utils'
import SearchBar from '../SearchBar.vue'

describe('searchBar', () => {
  it('renders properly', () => {
    const wrapper = mount(SearchBar)

    expect(wrapper.classes()).toContain('search-bar')
    expect(wrapper.find('input').exists()).toBe(true)
  })

  it('changes the model value when a search query is entered', async () => {
    const wrapper = mount(SearchBar)
    await wrapper.setProps({ modelValue: 'test' })

    expect(wrapper.props('modelValue')).toBe('test')
  })
})
