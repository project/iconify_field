interface Drupal {
  /** Translate a string */
  t: (key: string) => string
}

export default function useDrupal() {
  const app = getCurrentInstance()

  if (!app)
    throw new Error('useDrupal() must be called within a component')

  // Provide a default implementation for the Drupal object.
  // This is useful for unit tests.
  if (!app.appContext.config.globalProperties.Drupal) {
    app.appContext.config.globalProperties.Drupal = {
      t: (key: string) => key,
    }
  }

  return app.appContext.config.globalProperties.Drupal as Drupal
}
