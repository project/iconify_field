import { describe, expect, it } from 'vitest'
import useDrupal from '../useDrupal'

describe('useDrupal', () => {
  it('fails outside of a Vue context', () => {
    expect(() => useDrupal()).toThrowError(/^useDrupal\(\) must be called within a component$/)
  })
})
