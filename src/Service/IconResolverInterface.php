<?php

declare(strict_types=1);

namespace Drupal\iconify_field\Service;

use Drupal\Core\Template\Attribute;

/**
 * Resolves icons and collections from the Iconify library.
 *
 * @package Drupal\iconify_field\Service
 */
interface IconResolverInterface {

  /**
   * Get an SVG from an iconify field name.
   *
   * @param string $icon_name
   *   The icon name to get the icon from.
   * @param array|\Drupal\Core\Template\Attribute $attributes
   *   Additional attributes to add to the icon.
   * @param bool $autosize
   *   Whether to autosize the icon or keep it at 1em.
   *
   * @return array
   *   The SVG icon, as a render array.
   */
  public function getIcon(?string $icon_name, array|Attribute $attributes, bool $autosize = FALSE): array;

  /**
   * Get a list of available Iconify collections.
   *
   * @return array
   *   An array of available collections.
   */
  public function getIconifyCollections(): array;

  /**
   * Load a collection of icons from the Iconify library.
   *
   * @param string $collection_name
   *   The name of the collection to load.
   *
   * @return array
   *   An array representing the collection.
   */
  public function loadCollection(string $collection_name): array;

}
