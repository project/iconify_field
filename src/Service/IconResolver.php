<?php

declare(strict_types=1);

namespace Drupal\iconify_field\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Template\Attribute;
use Iconify\IconsJSON\Finder;

/**
 * {@inheritdoc}
 */
class IconResolver implements IconResolverInterface {

  /**
   * The Iconify icon finder.
   *
   * @var \Iconify\IconsJSON\Finder
   */
  protected $finder;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The loaded collections.
   *
   * @var array
   */
  protected $loadedCollections = [];

  /**
   * IconResolver constructor.
   */
  public function __construct(
    CacheBackendInterface $cache,
  ) {
    $this->finder = new Finder();
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function getIcon(?string $icon_name, array|Attribute $attributes = [], bool $autosize = FALSE) : array {
    $attrs = $attributes;

    if ($attrs instanceof Attribute) {
      $attrs = $attributes->toArray();
    }

    $cache_key = 'iconify_field:' . $icon_name;

    if ($attrs) {
      asort($attrs);
      $cache_key .= ':' . sha1(json_encode($attrs));
    }

    $cache_key .= ':' . (int) $autosize;

    if ($cache = $this->cache->get($cache_key)) {
      return $cache->data;
    }

    $fallback = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => $icon_name ?? '',
      '#attributes' => [
        'class' => [
          'iconify-field',
        ],
        ...$attrs,
      ],
    ];

    if (!$icon_name || strpos($icon_name, ':') === FALSE) {
      return $fallback;
    }

    [$collection_name, $icon_name] = explode(':', $icon_name);

    $collection = $this->loadCollection($collection_name);

    if (!$collection || !isset($collection['icons'][$icon_name])) {
      return $fallback;
    }

    $collection_width = $collection['width'];
    $collection_height = $collection['height'];
    $icon_width = $collection['icons'][$icon_name]['width'] ?? $collection_width;
    $normalized_width = round(($icon_width / $collection_width) * 100) / 100 . 'em';

    $icon_render = [
      '#type' => 'html_tag',
      '#tag' => 'svg',
      '#attributes' => [
        'xmlns' => 'http://www.w3.org/2000/svg',
        'class' => [
          'iconify-field',
        ],
        'width' => $autosize ? $icon_width : $normalized_width,
        'height' => $autosize ? $collection_height : '1em',
        'viewbox' => sprintf('0 0 %s %s', $icon_width, $collection_height),
        ...$attrs,
      ],
      '#value' => Markup::create($collection['icons'][$icon_name]['body']),
    ];

    $this->cache->set($cache_key, $icon_render);

    return $icon_render;
  }

  /**
   * {@inheritdoc}
   */
  public function getIconifyCollections(): array {
    $collections = [];

    $collections_data = Finder::collections();

    foreach ($collections_data as $collection_id => $collection) {
      $collection_name = sprintf('%s - %s', $collection['name'], $collection['license']['title']);
      $collections[$collection['category'] ?? 'Misc'][$collection_id] = $collection_name;
    }

    return $collections;
  }

  /**
   * {@inheritdoc}
   */
  public function loadCollection(string $collection_name): array {
    if (isset($this->loadedCollections[$collection_name])) {
      return $this->loadedCollections[$collection_name];
    }

    $collection_path = Finder::locate($collection_name);

    if (!file_exists($collection_path)) {
      return [];
    }

    $collection = json_decode(file_get_contents($collection_path), TRUE);

    if (!json_last_error() === JSON_ERROR_NONE) {
      return [];
    }

    $this->loadedCollections[$collection_name] = $collection;

    return $collection;
  }

}
