<?php

declare(strict_types=1);

namespace Drupal\iconify_field\Plugin\GraphQLCompose\FieldType;

use Drupal\graphql_compose\Plugin\GraphQL\DataProducer\FieldProducerTrait;
use Drupal\graphql_compose\Plugin\GraphQLCompose\GraphQLComposeFieldTypeBase;

/**
 * {@inheritdoc}
 *
 * @GraphQLComposeFieldType(
 *   id = "iconify_field_icon",
 *   type_sdl = "String",
 * )
 */
class IconItem extends GraphQLComposeFieldTypeBase {

  use FieldProducerTrait;

}
