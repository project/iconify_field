<?php

declare(strict_types=1);

namespace Drupal\iconify_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\iconify_field\Service\IconResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'icon_picker' widget.
 *
 * @FieldWidget(
 *   id = "iconify_field_icon_picker",
 *   label = @Translation("Icon Picker"),
 *   field_types = {
 *     "iconify_field_icon",
 *   }
 * )
 */
class IconPicker extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The IconResolver service instance.
   *
   * @var \Drupal\iconify_field\Service\IconResolverInterface
   */
  protected $iconResolver;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    IconResolverInterface $icon_resolver,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->iconResolver = $icon_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('iconify_field.icon_resolver'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'default_collection' => NULL,
      'collections' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $collections = $this->iconResolver->getIconifyCollections();

    $element['default_collection'] = [
      '#type' => 'select',
      '#title' => $this->t('Default collection'),
      '#description' => $this->t('The default collection to show.'),
      '#default_value' => $this->getSetting('default_collection'),
      '#options' => [
        NULL => $this->t('First available'),
        ...$collections,
      ],
    ];

    $element['collections'] = [
      '#type' => 'select',
      '#title' => $this->t('Allowed collections'),
      '#description' => $this->t('Select the icon collections that can be used. If none are selected, all collections will be available.'),
      '#default_value' => $this->getSetting('collections'),
      '#options' => $collections,
      '#multiple' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $collections = $this->getSetting('collections');

    $summary = [
      'default_collection' => $this->t('Default collection: @collection', [
        '@collection' => $this->getSetting('default_collection') ?: $this->t('First available'),
      ]),
      'collections' => $this->t('Allowed collections: @collections', [
        '@collections' => !empty($collections) ? implode(', ', $collections) : $this->t('All'),
      ]),
    ];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element['#attached'] = [
      'library' => [
        'iconify_field/icon-preview',
        'iconify_field/icon-picker',
      ],
    ];

    $element['value'] = $element + [
      '#type' => 'iconify_field',
      '#default_collection' => $this->getSetting('default_collection'),
      '#collections' => $this->getSetting('collections'),
      '#default_value' => $items[$delta]->value ?? '',
    ];

    $field_name = sprintf('%s[%s]', $items->getName(), $delta);

    $element['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
      '#collapsible' => TRUE,
      '#weight' => 999,
      'classes' => [
        '#type' => 'textfield',
        '#title' => $this->t('Additional classes'),
        '#description' => $this->t('Additional classes to add to the rendered icon'),
        '#default_value' => $items[$delta]->classes ?? '',
      ],
      'decorative' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Decorative image'),
        '#default_value' => $items[$delta]->decorative ?? TRUE,
      ],
      'arialabel' => [
        '#type' => 'textfield',
        '#title' => $this->t('Accessible name'),
        '#description' => $this->t('An accessible name for screen readers to use'),
        '#default_value' => $items[$delta]->arialabel ?? '',
        '#states' => [
          'visible' => [
            ':input[name="' . $field_name . '[advanced][decorative]"]' => [
              'checked' => FALSE,
            ],
          ],
        ],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$item) {
      $item['classes'] = $item['advanced']['classes'] ?? '';
      $item['decorative'] = $item['advanced']['decorative'] ?? TRUE;
      $item['arialabel'] = $item['advanced']['arialabel'] ?? '';

      if ($item['decorative']) {
        $item['arialabel'] = '';
      }
    }

    return $values;
  }

}
