<?php

declare(strict_types=1);

namespace Drupal\iconify_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'iconify_field_icon' formatter.
 *
 * @FieldFormatter(
 *   id = "iconify_field_icon_formatter",
 *   label = @Translation("Icon"),
 *   field_types = {
 *     "iconify_field_icon"
 *   }
 * )
 */
class IconField extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $classes = $item->get('classes')->getValue();
      $decorative = $item->get('decorative')->getValue();
      $arialabel = $item->get('arialabel')->getValue();

      $elements[$delta] = [
        '#type' => 'iconify_icon',
        '#icon' => $item->value,
        '#attributes' => [
          'class' => 'iconify-field ' . $classes ?? '',
        ],
      ];

      if ($decorative) {
        $elements[$delta]['#attributes']['aria-hidden'] = 'true';
      }

      if ($arialabel) {
        $elements[$delta]['#attributes']['aria-label'] = $arialabel;
        $elements[$delta]['#attributes']['role'] = 'img';
      }
    }

    return $elements;
  }

}
