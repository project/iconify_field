<?php

declare(strict_types=1);

namespace Drupal\iconify_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'icon' entity field type.
 *
 * @FieldType(
 *   id = "iconify_field_icon",
 *   label = @Translation("Icon"),
 *   description = @Translation("Field to store Iconify icon names."),
 *   default_widget = "iconify_field_icon_picker",
 *   default_formatter = "iconify_field_icon_formatter"
 * )
 */
class IconItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'normal',
          'not null' => TRUE,
        ],
        'classes' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'default' => '',
        ],
        'decorative' => [
          'type' => 'int',
          'size' => 'tiny',
          'not null' => TRUE,
          'default' => TRUE,
        ],
        'arialabel' => [
          'type' => 'text',
          'size' => 'normal',
          'not null' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Value'))
      ->setRequired(TRUE);
    $properties['classes'] = DataDefinition::create('string')
      ->setLabel(t('Additional classes'))
      ->setRequired(FALSE);
    $properties['decorative'] = DataDefinition::create('boolean')
      ->setLabel(t('Is decorative'))
      ->setRequired(FALSE);
    $properties['arialabel'] = DataDefinition::create('string')
      ->setLabel(t('Accessible name'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'value' => '',
      'classes' => '',
      'decorative' => TRUE,
      'arialabel' => '',
    ] + parent::defaultFieldSettings();
  }

}
