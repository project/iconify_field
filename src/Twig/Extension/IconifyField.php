<?php

declare(strict_types=1);

namespace Drupal\iconify_field\Twig\Extension;

use Drupal\Core\Template\Attribute;
use Drupal\iconify_field\Service\IconResolverInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * IconifyField Twig extension.
 */
class IconifyField extends AbstractExtension {

  /**
   * The IconResolver service instance.
   *
   * @var \Drupal\iconify_field\Service\IconResolverInterface
   */
  protected $iconResolver;

  /**
   * IconifyField constructor.
   */
  public function __construct(IconResolverInterface $iconResolver) {
    $this->iconResolver = $iconResolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('iconify_field', $this->iconifyField(...)),
    ];
  }

  /**
   * Get an SVG from an iconify field name.
   *
   * @param string $field
   *   The field to get the icon from.
   * @param array|\Drupal\Core\Template\Attribute $attributes
   *   Additional attributes to add to the icon.
   *
   * @return string
   *   The SVG icon.
   */
  public function iconifyField(?string $field, array|Attribute $attributes = []) {
    $icon = $this->iconResolver->getIcon($field, $attributes);

    return $icon;
  }

}
