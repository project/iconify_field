<?php

namespace Drupal\iconify_field\Controller;

use Drupal\Core\Controller\ControllerBase;
use Iconify\IconsJSON\Finder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Get iconify icons of a collection.
 */
class IconifyFieldApiController extends ControllerBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * IconifyFieldApiController constructor.
   */
  public function __construct(Request $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Get iconify icons.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $collection
   *   The collection to get icons from.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response.
   */
  public function icons(Request $request, string $collection): JsonResponse {
    $json_path = Finder::locate($collection);

    if (!file_exists($json_path)) {
      return new JsonResponse([
        'error' => 'Collection not found',
        'icons' => [],
      ]);
    }

    $json = file_get_contents($json_path);
    $icon_data = json_decode($json, TRUE);

    foreach ($icon_data['icons'] as $name => $icon) {
      $icons[] = $collection . ':' . $name;
    }

    return new JsonResponse([
      'error' => NULL,
      'icons' => $icons,
    ]);
  }

  /**
   * Get iconify collections.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response.
   */
  public function collections(): JsonResponse {
    $collections = Finder::collections();

    $collections_filter_query = $this->request->query->get('collections') ?? '';
    $collections_filter = array_filter(explode(',', $collections_filter_query));

    if (!empty($collections_filter)) {
      $collections = array_intersect_key($collections, array_flip($collections_filter));
    }

    return new JsonResponse([
      'error' => NULL,
      'collections' => $collections,
    ]);
  }

}
