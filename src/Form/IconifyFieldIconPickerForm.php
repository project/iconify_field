<?php

declare(strict_types=1);

namespace Drupal\iconify_field\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * A form where a user can pick an icon.
 */
class IconifyFieldIconPickerForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'iconify_field_icon_picker_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['picker'] = [
      '#theme' => 'iconify_field_icon_picker',
      '#dev_mode' => !!getenv('ICONIFY_FIELDS_DEV_MODE'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
  }

}
