<?php

declare(strict_types=1);

namespace Drupal\iconify_field\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element for selecting an Iconify icon.
 *
 * @RenderElement("iconify_icon")
 */
class IconifyIcon extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;

    return [
      '#input' => TRUE,
      '#pre_render' => [
        [$class, 'preRenderIconifyField'],
      ],
      '#theme' => 'iconify_icon',
      '#attached' => [
        'library' => [
          'iconify_field/icon',
        ],
      ],
    ];
  }

  /**
   * Prepare the element for rendering.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *
   * @return array
   *   The processed element.
   */
  public static function preRenderIconifyField($element) {
    return $element;
  }

}
