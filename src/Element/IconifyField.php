<?php

declare(strict_types=1);

namespace Drupal\iconify_field\Element;

use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a form element for selecting an Iconify icon.
 *
 * @FormElement("iconify_field")
 */
class IconifyField extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;

    return [
      '#input' => TRUE,
      '#pre_render' => [
        [$class, 'preRenderIconifyField'],
      ],
      '#theme' => 'input__iconify_field',
      '#theme_wrappers' => [
        'form_element',
      ],
    ];
  }

  /**
   * Prepare the element for rendering.
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *
   * @return array
   *   The processed element.
   */
  public static function preRenderIconifyField($element) {
    $element['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $element['#attached']['library'][] = 'iconify_field/icon-preview';
    $element['#attached']['library'][] = 'iconify_field/icon-picker';

    return $element;
  }

}
