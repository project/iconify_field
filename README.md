# Iconify Field

The Iconify Field module for Drupal allows you to easily add SVG icons to any type of content, using the amazing Iconify project.

## What does it do?

In short, it provides a new field type for selecting icons from any collection of Iconify icons. The resulting icon or icons will be rendered in SVG form on the frontend, without the need for any client-side JavaScript.

## Can it do anything else?

It can! Of course, just providing a field type for one or more icons can quickly become very limiting. That is why some helpers are provided alongside the module.

### Twig function

You might run into situations where you have custom functionality, and want to embed a single icon in a Twig template. That's where the iconify_field Twig function comes in. Simply provide the collection and name of the icon, and the correct icon will be rendered.

For example:

```twig
{{ iconify_field('mdi:user') }}
```

### The icon picker is pretty nice, can I use it in my custom form?

Be my guest! It's quite simply to include, since a custom form element is provided with this module.

Here's a code snippet to get you started:

```php
$form['icon'] = [
  '#type' => 'iconify_field',
  '#default_collection' => 'mdi', // Can also be NULL, in which case the first collection will be pre-selected
  '#collections' => [
    'mdi',
    'tabler',
  ], // Can also be empty, in which case all collections are allowed
  '#default_value' => 'mdi:user',
];
```
