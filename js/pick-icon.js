Drupal.behaviors.IconifyFieldPickIconBehaviour = {
  attach: function (context) {

    once('IconifyFieldPickIconBehaviour', document.body, context).forEach(
      (element) => {
        element.addEventListener('iconify-field-pick-icon', (event) => {
          const form = element.querySelector('.iconify-field-icon-picker-form')

          // If the form is not found, return early.
          if (!form || !form.action)
            return

          const actionUrl = new URL(form.action)

          // If the action URL does not have a search query, return early.
          if (!actionUrl.search)
            return

          const selector = new URLSearchParams(actionUrl.search).get('selector')
          const field = document.querySelector(`[data-drupal-selector="${selector}"]`)

          // If the field is not found, return early.
          if (!field)
            return

          const input = field.querySelector('input')
          const preview = field.querySelector('.iconify-field--icon-preview > .iconify-field')
          const label = field.querySelector('.iconify-field--icon-label')

          input.value = event?.detail?.text ?? ''
          preview.outerHTML = event?.detail?.icon || '<span class="iconify-field"></span>'
          label.textContent = event?.detail?.text || Drupal.t('No icon selected')

          element.querySelector('.ui-dialog-titlebar-close').click()
        })

        const clearBtns = element.querySelectorAll('.iconify-field--icon-clear')

        clearBtns.forEach((clearBtn) => {
          const field = clearBtn.closest('.iconify-field--wrapper')

          clearBtn.addEventListener('click', (event) => {
            event.preventDefault()

            const input = field.querySelector('input')
            const preview = field.querySelector('.iconify-field--icon-preview > .iconify-field')
            const label = field.querySelector('.iconify-field--icon-label')

            input.value = ''
            preview.outerHTML = '<span class="iconify-field"></span>'
            label.textContent = Drupal.t('No icon selected')
          })
        })
      }
    )

  }
}
